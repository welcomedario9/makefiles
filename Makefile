# Variables
OBJS = main.o calculadora.o salida.o
BINARY = programa

CFLAGS = -g -Wall 
CC = clang

# Variables de expansión recursiva y simple
x = Hola
y := $(x), Adiós!
x = Adiós

# Regla para obtener conmo objetivo programa principal.
programa: $(OBJS)
	clang -o $(BINARY) $(OBJS)

# Reglas Implícitas
main.o: main.c funciones.h
	
salida.o: salida.c funciones.h

calculadora.o: calculadora.c

# Phony Rules | Reglas fictícias
clean:
	rm -f $(BINARY) $(OBJS)

imprimir:
	@echo $(x)
	@echo $(y)
