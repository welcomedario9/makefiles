# Tutorial de Make

**Make** es una herramienta que sirve de apoyo a la hora de compilar programas
de ordenador. Simplifica mucho el proceso de compilación en programas grandes.
Distingue lo se ha modificado en cada archivo y solamente compila lo que ha sido
modificado.

Se utiliza un archivo de Makefile para declarar las distintas reglas de 
compilación que deseemos aplicar sobre nuestros archivos fuente.

## Reglas, objetivos, dependencias

Un Makefile es un archivo en el que vamos a declarlar una serie de reglas que
que nos va a decir como podemos hacer algo: Como enlazar un programa, como 
compilar un archivo de código fuente en código objeto, como empaquetar los .c en
un archivador .tar, como eliminar los archivos temporales. Todo eso lo declaramos
dentro de un archivo Makefile.

En un Makefile se utilizarán tres cosas:

objetivo: dependencias
    intrucciones

objetivo: dependencias
    intrucciones

objetivo: dependencias
    intrucciones

Siempre será lo mismo, solo que en cada caso utilizaremos un objetivo distintor.
 
**Objetivo** va ser lo que queremos obtener: binario, .o, .a, .so, etc; que 
queremos obtener a partir de la ejecución de make para procesar una determinada
regla.

**Dependencias** van a ser esos archivos que ya es necesario que existan antes 
que se pueda procesar ese objetivo. Por ejemplo, para obtener un binario es
es necesario tener los archivos objetos necesarios .o, estas vendrían a ser las
dependencias de ese objetivo binario. 

**Instrucciones** son las intrucciones de codigo que se tienen que ejecutar sobre
las dependencias para obtener ese objetivo: Compilar una librería, borrar un 
directorio, enlazar varios .o, etc.

**Reglas** vendría a ser la aplicación de todo lo anterior. Un archivo Makefile
lo que contiene son las distintas reglas. 

Por ejemplo: Podriamos crear una regla cuyo objetivo sea programa, cuyas 
dependencias sean main.o calculadora.o & salida.o, que ejecute como intrucciones 
el comando para enlazar todas las dependencias. 

A su vez cada dependencia podría ser una regla independiente donde el objetivo 
sea como construir esa dependencia. Sus dependencias a su vez serían los .c, .h,
etc, en el caso de necesitar un archivo .o (objeto) 

## Reglas ficticias (Phony rules)

Existen alguanas reglas especiales que no nesitan que pongamos nada, que podemos
poner tal cual sin ninguna dependencia y que son reglas ficticias que simplemente
ejecutan comandos o intrucciones.

Por ejemplo:

clean:
    rm -f programa *.o 

Por convención se escribe una regla clean para limpiar todos los archivos innecesarios.
Otra regla que se usa por convención es crear una regla principla (all) en la que 
pondremos las distintas reglas que queremos que se ejecuten si no pasamos nada.

Por ejemplo:

all: programa

programa: main.o salida.o calculadora.o 
    cc -o programa main.o salida.o calculadora.o

## Variables

En Make se introduce el concepto de variable que es básicamente asginarle un nombre
a un valor, para luego utilizar ese nombre reservado para referirnos a ese valor. 

La forma de declarar una variable es simplemente, escribir el nombre "Por convención
se escriben los nombres de variables en MAYUSCULAS" de esa variable seguido del signo
igual "=", y a la derecha del igual asignarle una valor.

Para acceder al valor contenido en una variable se utiliza el signo de dolar seguido
de parentesis y dentro el nombre de la variable. $(VAR)

Por ejemplo:

OBJETOS = main.o calculadora.o salida.o

programa: $(OBJETOS)
    clang -o programa $(OBJETOS)

clean:
    rm -f programa $(OBJETOS)

Las variables tienen alguanas características especiales, se puede almacenar el valor
de una variable dentro de otra.

Por ejemplo:

x = Hola
y = $(x), Adiós

imprimir:
    echo $(x)
    echo $(y)

Make siempre nos muestra los comandos que está ejecutando, sin emabargo si queremos 
que no muestre lo que hace podemos anteponer el signo de @ antes de las intrucciones,
de esta forma Make trabajará silenciosamente.

Por ejemplo:

imprimir:
    @echo $(x)
    @echo $(y)

### Variables de expansión recursiva versus expansión simple

Las variables de expansión recursiva lo que hacen es que en la variable se almacena la 
referencia a otra variable. Cada vez que intentemos acceder a ella, se va a resolver lo 
que valga la variable en ese momento.

Por ejemplo:

x = Hola
y = $(x) Adiós
x = Adiós

imprimir:
    @echo $(y)

$ Adiós Adiós

Las variables de expansión simple hace la expansión de la variables en el mismo momento
que es asignada. Para declarar una variable de expansión simple tenemos que anteponer un
signo de : antes del igual en la asignación, quedando de la siguiente manera, :=

Por ejemplo:

x = Hola
y := $(x), Adiós
x = Adiós

imprimir:
    @echo $(y)

$ Hola, Adiós

## Reglas Implícitas (Built-In Rules)

Las reglas implícitas se basan en que Make ya sabe como compilar y enlazar algunos tipos
especiales de archivos. El catálogo de reglas esta en la documentación oficial de Make.

Al detectar un sufíjo, el ya sabe exactamente que hacer para un sin fin de lenguajes, 
entonces podriamos poner nuestro código de la siguiente manera.

Por ejemplo:

main.o:  main.c funciones.h
salida.o: salida.c funciones.h
calculadora.o: calculadora.c 

